<?php
/**
 *
 * @author		Bibash
 * @version      16.03.27
 *
 */
 class Database
{
    # @object, The PDO object
    private $pdo;
    
    # @object, PDO statement object
    private $sQuery;
    
    # @array,  The database settings
    private $settings;
    
    # @bool ,  Connected to the database
    private $bConnected = false;
    
    # @object, Object for logging exceptions	
    private $log;
    
    # @array, The parameters of the SQL query
    private $parameters;
    
    # @int, Last Inserted id value
    private $lastInsertId;
    
    private $host;
    private $port;
    private $socket;
    private $userName;
    private $passWord;
    private $dataBase;
    
    /**
     *   Default Constructor 
     *
     *	1. Instantiate Log class.
     *	2. Connect to database.
     *	3. Creates the parameter array.
     */
    public function __construct()
    {
        //$this->log = new Log();
        //$this->OpenConnection();
        $this->parameters = array();
        
        $this->host =  "localhost";
        $this->userName = "bibash_root";
        $this->passWord = "pwd@r00t";
        $this->dataBase = "bibash_passbook";
    }
    
    /**
     *	This method makes connection to the database.
     */
    private function OpenConnection()
    {
        $dsn = "mysql:host=".$this->host.";dbname=".$this->dataBase.";charset=utf8";
        try {
            # Read settings from INI file, set UTF8
            $this->pdo = new PDO($dsn, $this->userName, $this->passWord, array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ));
            
            # We can now log any exceptions on Fatal error. 
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            # Disable emulation of prepared statements, use REAL prepared statements instead.
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            
            # Connection succeeded, set the boolean to true.
            $this->bConnected = true;
        }
        catch (PDOException $e) {
            # Write into log
            echo $this->ExceptionLog($e->getMessage());
            die();
        }
    }
    /*
     *   You can use this little method if you want to close the PDO connection
     *
     */
    private function CloseConnection()
    {
        # Set the PDO object to null to close the connection
        $this->pdo = null;
        $this->bConnected = false;
    }
    
    /**
     *	Every method which needs to execute a SQL query uses this method.
     *	
     *	1. If not connected, connect to the database.
     *	2. Prepare Query.
     *	3. Parameterize Query.
     *	4. Execute Query.	
     *	5. On exception : Write Exception into the log + SQL query.
     *	6. Reset the Parameters.
     */
    private function Init($query, $parameters = "")
    {
        # Connect to database
        if (!$this->bConnected) {
            $this->OpenConnection();
        }
        try {
            # Prepare query
            $this->sQuery = $this->pdo->prepare($query);

            # Add parameters to the parameter array	
            $this->bindMore($parameters);
            
            # Bind parameters
            if (!empty($this->parameters)) {
                foreach ($this->parameters as $param => $value) {
                    
                    $type = PDO::PARAM_STR;
                    switch ($value[1]) {
                        case is_int($value[1]):
                            $type = PDO::PARAM_INT;
                            break;
                        case is_bool($value[1]):
                            $type = PDO::PARAM_BOOL;
                            break;
                        case is_null($value[1]):
                            $type = PDO::PARAM_NULL;
                            break;
                    }
                    // Add type when binding the values to the column
                    $this->sQuery->bindValue($value[0], $value[1], $type);
                }
            }
            
            # Execute SQL 
            $this->sQuery->execute();
        }
        catch (PDOException $e) {
            # Write into log and display Exception
            echo $this->ExceptionLog($e->getMessage(), $query);
            die();
        }
        
        # Reset the parameters
        $this->parameters = array();
    }
    
    /**
     *	@void 
     *
     *	Add the parameter to the parameter array
     *	@param string $para  
     *	@param string $value 
     */
    public function Bind($para, $value)
    {
        $this->parameters[sizeof($this->parameters)] = [":" . $para , $value];
    }
    /**
     *	@void
     *	
     *	Add more parameters to the parameter array
     *	@param array $parray
     */
    public function BindMore($parray)
    {
        if (empty($this->parameters) && is_array($parray)) {
            $columns = array_keys($parray);
            foreach ($columns as $i => &$column) {
                $this->bind($column, $parray[$column]);
            }
        }
    }
    /**
     *  If the SQL query  contains a SELECT or SHOW statement it returns an array containing all of the result set row
     *	If the SQL statement is a DELETE, INSERT, or UPDATE statement it returns the number of affected rows
     *
     *  @param  string $query
     *	@param  array  $params
     *  @param  bool   $keepConnestionOpen
     *	@return void
     */
    public function ExecuteQuery($query, $params = null, $keepConnestionOpen = false)
    {
        try{
              if (!$this->bConnected) {
                     $this->OpenConnection();
                }

                $query = trim(str_replace("\r", " ", $query));
                $this->Init($query, $params);
                $this->sQuery->rowCount();
                //$this->sQuery->execute();
                $this->lastInsertId = $this->pdo->lastInsertId();
                if(! $keepConnestionOpen)
                    $this->CloseConnection();
        }
        catch (PDOException $e) {
            # Write into log and display Exception
            echo $this->ExceptionLog($e->getMessage(), $query);
            die();
        }       
    }

     /**
     *  Returns an array which represents a dataset of multiple query from the result set 
     *
     *  @param  string $query
     *  @param  array  $params
     *  @param  int    $fetchmode
     *  @return array
     */

    public function ExecuteDataSet($query, $params = null, $fetchmode = PDO::FETCH_ASSOC){
        $result = array ();
        $list = explode(";",$query);
        foreach( $list as $sql ) {
            if(!empty($sql))
                $result[]= $this->ExecuteDataTable($sql, $params, $fetchmode);            
        }
        return $result;
    }

    /**
     *  Returns an array which represents a table from the result set 
     *
     *  @param  string $query
     *  @param  array  $params
     *  @param  int    $fetchmode
     *  @return array
     */
    public function ExecuteDataTable($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
    {
        if (!$this->bConnected) {
            $this->OpenConnection();
        }
        $this->Init($query, $params);
        $result = $this->sQuery->fetchAll($fetchmode); 
        $this->sQuery->closeCursor(); // Frees up the connection to the server so that other SQL statements may be issued,
        $this->CloseConnection();
        return $result;
    }

    /**
     *  Returns an array which represents a row from the result set 
     *
     *  @param  string $query
     *  @param  array  $params
     *      @param  int    $fetchmode
     *  @return array
     */
    public function ExecuteDataRow($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
    {
        if (!$this->bConnected) {
            $this->OpenConnection();
        }
        $this->Init($query, $params);
        $result = $this->sQuery->fetch($fetchmode);
        $this->sQuery->closeCursor(); // Frees up the connection to the server so that other SQL statements may be issued,
        $this->CloseConnection();
        return $result;
    }
    /**
     *  Returns the value of one single field/column
     *
     *  @param  string $query
     *  @param  array  $params
     *  @return string
     */
    public function GetSingleResult($query, $params = null)
    {
        if (!$this->bConnected) {
            $this->OpenConnection();
        }
        $this->Init($query, $params);
        $result = $this->sQuery->fetchColumn();
        $this->sQuery->closeCursor(); // Frees up the connection to the server so that other SQL statements may be issued
        $this->CloseConnection();
        return $result;
    }
    
    /**
     *  Returns the last inserted id.
     *  @return string
     */
    public function GetLastInsertId()
    {
        return $this->lastInsertId;
    }
    
    /**
     * Starts the transaction
     * @return boolean, true on success or false on failure
     */
    public function BeginTransaction()
    {
        return $this->pdo->beginTransaction();
    }
    
    /**
     *  Execute Transaction
     *  @return boolean, true on success or false on failure
     */
    public function CommitTransaction()
    {
        return $this->pdo->commit();
    }
    
    /**
     *  Rollback of Transaction
     *  @return boolean, true on success or false on failure
     */
    public function RollBackTransaction()
    {
        return $this->pdo->rollBack();
    }
    
    /**
     *	Returns an array which represents a column from the result set 
     *
     *	@param  string $query
     *	@param  array  $params
     *	@return array
     */
    public function GetColumn($query, $params = null,$fetchmode = PDO::FETCH_NUM)
    {
    	if (!$this->bConnected) {
    		$this->OpenConnection();
    	}
        $this->Init($query, $params);
        $Columns = $this->sQuery->fetchAll($fetchmode);
        $this->CloseConnection();
        $column = null;
        
        foreach ($Columns as $cells) {
            $column[] = $cells[0];
        }
        
        return $column;
        
    }
     
    /**	
     * Writes the log and returns the exception
     *
     * @param  string $message
     * @param  string $sql
     * @return string
     */
    private function ExceptionLog($message, $sql = "")
    {
        $exception = 'Unhandled Exception. <br />';
        $exception .= $message;
        $exception .= "<br /> You can find the error back in the log.";
        
        if (!empty($sql)) {
            # Add the Raw SQL to the Log
            $message .= "\r\nRaw SQL : " . $sql;
        }
        # Write into log
        //$this->log->write($message);
        $result["isSuccess"]= false;
        $result["message"] = $message;
        return json_encode($result);
    }
}
?>
