<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'config/database.php';

$app = new \Slim\App;

$app->get('/api/get', function (Request $request, Response $response) {
   $db = new Database();
   $result = $db->ExecuteDataTable("select id, sysInfo, userName, pwd, url, remarks, createdDate from bibash_passbook.paassbook where isDeleted = 0");

   header("Content-Type: application/json");
   echo json_encode($result);
});

$app->get('/api/get/{id}', function (Request $request, Response $response) {
   $id = $request->getAttribute("id");

   $db = new Database();
   $result = $db->ExecuteDataTable("select id, sysInfo, userName, pwd, url, remarks, createdDate from paassbook where isDeleted = 0 and id = $id");

   header("Content-Type: application/json");
   echo json_encode($result);
});

$app->post('/api/add', function (Request $request, Response $response) {
   $sysInfo = $request->getParam("sysInfo");
   $userName = $request->getParam("userName");
   $pwd = $request->getParam("pwd");
   $url = $request->getParam("url");
   $remarks = $request->getParam("remarks");

   $db = new Database();
   $sql = "insert into paassbook(sysInfo, userName, pwd, url, remarks, createdDate) select '$sysInfo', '$userName', '$pwd', '$url', '$remarks', now()";

   $db->ExecuteQuery($sql);
   $result["IsSucess"] = true;
   $result["Id"] = $db->GetLastInsertId();
   $result["Message"] = "Data Added Successfully";

   header("Content-Type: application/json");
   echo json_encode($result);
});

$app->put('/api/update', function (Request $request, Response $response) {

   $id = $request->getParam("id");
   $sysInfo = $request->getParam("sysInfo");
   $userName = $request->getParam("userName");
   $pwd = $request->getParam("pwd");
   $url = $request->getParam("url");
   $remarks = $request->getParam("remarks");

   $db = new Database();
   $sql = "update paassbook set sysInfo = '$sysInfo', userName = '$userName', pwd = '$pwd', url = '$url', remarks = '$remarks', modifiedDate = now() where id = $id";

   $db->ExecuteQuery($sql);
   $result["IsSucess"] = true;
   $result["Message"] =  "Data updated successfully";
   header("Content-Type: application/json");
   echo json_encode($result);

});

$app->put('/api/delete', function (Request $request, Response $response) {

   $id = $request->getParam("id");
  
   $db = new Database();
   $sql = "update paassbook set isDeleted = 1, modifiedDate = now() where id = $id";
   $db->ExecuteQuery($sql);

   $result["IsSucess"] = true;
   $result["Message"] = "Data deleted successfully";
   header("Content-Type: application/json");
   echo json_encode($result);

});